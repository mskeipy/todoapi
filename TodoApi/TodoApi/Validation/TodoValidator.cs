﻿using System.Data;
using FluentValidation;
using TodoApi.Models;

namespace TodoApi.Validation
{
    public class TodoValidation : AbstractValidator<TodoItem>
    {
        public TodoValidation()
        {
            RuleFor(TodoItem => TodoItem.Id).NotNull();
            RuleFor(TodoItem => TodoItem.Name).NotNull();
            RuleFor(TodoItem => TodoItem.Name).NotEmpty();
        }
    }
}