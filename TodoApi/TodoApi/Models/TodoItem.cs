﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace TodoApi.Models
{
    public class TodoItem
    {
        public long Id { get; set; }
        
        [Required]
        [PersonalData]
        public string Name { get; set; }
        public bool IsComplete { get; set; }
    }
}